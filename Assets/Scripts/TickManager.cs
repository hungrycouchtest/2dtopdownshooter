﻿using System;
using TopDownShooter.Misc;

namespace TopDownShooter
{
    public sealed class TickManager : Singleton<TickManager>
    {
        #region Events
        public event Action OnTick = () => { };
        public event Action OnFixedTick = () => { };
        #endregion

        #region Unity Methods
        private void Update()
        {
            OnTick.Invoke();
        }

        private void FixedUpdate()
        {
            OnFixedTick.Invoke();
        }
        #endregion
    }
}
