﻿using UnityEngine;

namespace TopDownShooter.Misc
{
    public abstract class Updatable : MonoBehaviour, IUpdatable
    {
        private void OnEnable()
        {
            TickManager.getInstance.OnTick += FastUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnTick -= FastUpdate;
        }

        public abstract void FastUpdate();
        public abstract void FastFixedUpdate();
    }
}