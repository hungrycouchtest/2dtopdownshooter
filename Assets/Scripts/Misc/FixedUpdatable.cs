﻿using UnityEngine;

namespace TopDownShooter.Misc
{
    public abstract class FixedUpdatable : MonoBehaviour, IUpdatable
    {
        private void OnEnable()
        {
            TickManager.getInstance.OnFixedTick += FastUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnFixedTick -= FastUpdate;
        }

        public abstract void FastUpdate();
        public abstract void FastFixedUpdate();
    }
}