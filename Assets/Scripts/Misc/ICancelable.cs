﻿using System.Threading;

namespace TopDownShooter.Misc
{
    public interface ICancelable
    {
        CancellationToken GetCancellationToken { get; }
    }
}