﻿using TopDownShooter.Misc;
using TopDownShooter.UI;
using UnityEngine;

namespace TopDownShooter
{
    public sealed class UIManager : Singleton<UIManager>
    {
        #region Fields
        [Header("Components")]
        [SerializeField]
        private Camera _mainCamera;
        [SerializeField]
        private GameObject _scoreController;
        #endregion

        #region Props
        public IScoreController GetScoreController { get; private set; }
        public Camera GetMainCamera => _mainCamera;
        #endregion


        #region Private Methods
        private void Init()
        {
            GetScoreController = _scoreController.GetComponent<IScoreController>();
        }
        #endregion

        #region Unity Methods
        private void Start()
        {
            Init();
        }
        #endregion
    }
}