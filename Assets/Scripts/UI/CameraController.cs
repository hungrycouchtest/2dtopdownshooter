﻿using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.UI
{
    public class CameraController : MonoBehaviour, IUpdatable
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _followSpeed = 4.5f;
        #endregion


        #region Private Methods
        private void Init()
        {
            transform.position = new Vector3(0, 0, transform.position.z);
        }
        #endregion

        #region Unity Methods
        private void Start()
        {
            Init();
        }

        private void OnEnable()
        {
            TickManager.getInstance.OnFixedTick += FastFixedUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnFixedTick -= FastFixedUpdate;
        }
        #endregion

        #region Public Methods
        public void FastFixedUpdate()
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3((float)GameManager.getInstance.GetPlayer?.Position.x, (float)GameManager.getInstance.GetPlayer?.Position.y, transform.position.z), Time.fixedDeltaTime * _followSpeed);
        }

        public void FastUpdate()
        {

        }
        #endregion
    }
}