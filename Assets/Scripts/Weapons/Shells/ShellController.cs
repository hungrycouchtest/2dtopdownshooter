﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopDownShooter.Characters;
using UnityEngine;

namespace TopDownShooter.Weapons
{
    public class ShellController : MonoBehaviour, IShellController
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private int _damage = 5;
        [SerializeField]
        private float _forceSpeed = 20f;
        [SerializeField]
        private float _lifeTime = 5;

        [Header("Components")]
        [SerializeField]
        private Rigidbody2D _rigidbody2D;
        #endregion


        #region Props
        public int GetDamage => _damage;
        public float GetForceSpeed => _forceSpeed;
        public float GetLifeTime => _lifeTime;
        #endregion



        #region Private Methods
        private void Init()
        {
            _rigidbody2D.AddForce(transform.up * _forceSpeed, ForceMode2D.Impulse);
            DestroyShell(false);
        }
        #endregion

        #region UnityMethods
        private void Start()
        {
            Init();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            collision.transform.GetComponentInParent<IDamageable>()?.Damage(_damage);
            DestroyShell();
        }
        #endregion

        #region PublicMethods
        public void DestroyShell(bool instant = true)
        {
            if (instant)
                Destroy(gameObject);
            else
                Destroy(gameObject, _lifeTime);
        }
        #endregion
    }
}
