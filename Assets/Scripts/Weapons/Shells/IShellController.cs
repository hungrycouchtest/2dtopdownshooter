﻿
namespace TopDownShooter.Weapons
{
    public interface IShellController
    {
        int GetDamage { get; }
        float GetForceSpeed { get; }
        float GetLifeTime { get; }

        void DestroyShell(bool instant = true);
    }
}