﻿using System;
using System.Threading;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Weapons
{
    public class BeamGun : MonoBehaviour, IWeapon, IUpdatable, IBeamWeaponAdapter, ICancelable
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _fireCooldown = 5f;
        [SerializeField]
        private float _fireTime = 2f;
        [SerializeField]
        private float _fireDamageInterval = 0.1f;
        [SerializeField]
        private float _cooldown = 0;
        [SerializeField]
        private int _damage = 1;
        [SerializeField]
        private int _ammo = 0;
        [Header("Components")]
        [SerializeField]
        private LineRenderer _lineRenderer;


        private FireController fireController;
        private BeamController beamController;
        private CancellationTokenSource cts;
        #endregion

        #region Events
        public event Action OnReload;
        #endregion

        #region Props
        public float GetFireCooldown => _fireCooldown;
        public float GetFireTime => _fireTime;
        public float GetCooldown => _cooldown;
        public int GetDamage => _damage;
        public int GetAmmo => _ammo;
        public float GetDamageInterval => _fireDamageInterval;
        public CancellationToken GetCancellationToken => cts.Token;
        public bool IsActive { get => gameObject.activeSelf; set => gameObject.SetActive(value); }
        #endregion



        #region Private Methods
        private void Init()
        {
            fireController = new FireController(this, OnReload, this);
            beamController = new BeamController(this, _lineRenderer, this);
            _lineRenderer.gameObject.SetActive(false);
        }


        #endregion

        #region Unity Methods
        private void Start()
        {
            Init();
        }

        private void OnEnable()
        {
            cts = new CancellationTokenSource();
            TickManager.getInstance.OnTick += FastUpdate;
            TickManager.getInstance.OnFixedTick += FastFixedUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnTick -= FastUpdate;
            TickManager.getInstance.OnFixedTick -= FastFixedUpdate;
            cts.Cancel();
            cts.Dispose();
        }

        private void OnDestroy()
        {
            if (!cts.IsCancellationRequested)
                cts.Cancel();
            cts.Dispose();
        }
        #endregion

        #region Public Methods
        public void Shoot()
        {
            if (!fireController.Fire())
                return;

            beamController.ActivateLaser();
        }

        public void FastUpdate()
        {
            beamController?.FastUpdate();
        }

        public void FastFixedUpdate()
        {
            beamController?.FastFixedUpdate();
        }
        #endregion
    }
}