﻿
namespace TopDownShooter.Weapons
{
    public interface IBeamWeaponAdapter : IWeapon
    {
        float GetFireTime { get; }
        float GetDamageInterval { get; }
    }
}
