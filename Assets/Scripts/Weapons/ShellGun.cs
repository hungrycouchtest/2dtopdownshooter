﻿using System;
using System.Threading;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter.Weapons
{
    public class ShellGun : MonoBehaviour, IWeapon, ICancelable
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _fireCooldown = 0.5f;
        [SerializeField]
        private float _cooldown = 5f;
        [SerializeField]
        private int _damage = 5;
        [SerializeField]
        private int _ammo = 10;
        [SerializeField]
        private Transform _firePoint;

        [Header("Prefabs")]
        [SerializeField]
        private GameObject _shellPrefab;


        private FireController fireController;
        private CancellationTokenSource cts;
        private Transform shellsFolder;
        #endregion

        #region
        public event Action OnReload;
        #endregion

        #region Props
        public float GetFireCooldown => _fireCooldown;
        public float GetCooldown => _cooldown;
        public int GetDamage => _damage;
        public int GetAmmo => _ammo;
        public CancellationToken GetCancellationToken => cts.Token;
        public bool IsActive { get => gameObject.activeSelf; set => gameObject.SetActive(value); }
        #endregion



        #region Private Methods
        private void Init()
        {
            cts = new CancellationTokenSource();
            fireController = new FireController(this, OnReload, this);
            shellsFolder = new GameObject("ShellsFolder").transform;
        }
        #endregion

        #region Unity Methods
        private void Start()
        {
            Init();
        }

        private void OnDestroy()
        {
            if (!cts.IsCancellationRequested)
                cts.Cancel();
            cts.Dispose();
        }
        #endregion

        #region Public Methods
        public void Shoot()
        {
            if (!fireController.Fire())
                return;

            Factory.getInstance.Create<IShellController>(_shellPrefab, shellsFolder, _firePoint.transform);
        }
        #endregion
    }
}