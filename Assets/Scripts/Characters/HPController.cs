﻿using System;

namespace TopDownShooter.Characters
{
    internal class HPController
    {
        #region Fields
        private readonly Multiped multiped;
        private readonly Action dieCallback;
        private int currentHP;
        private bool isDead;
        #endregion


        public HPController(Multiped multiped, Action dieCallback = null)
        {
            this.multiped = multiped;
            this.dieCallback = dieCallback;
            this.currentHP = multiped.GetHP;
            this.isDead = false;
        }


        #region Methods
        public void Damage(int damage)
        {
            if (isDead)
                return;

            currentHP -= damage;

            if (currentHP < 1)
            {
                isDead = true;
                dieCallback?.Invoke();
            }
        }
        #endregion
    }
}