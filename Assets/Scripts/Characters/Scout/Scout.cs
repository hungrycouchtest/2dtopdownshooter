using System;
using TopDownShooter.Misc;
using TopDownShooter.Weapons;
using UnityEngine;

namespace TopDownShooter.Characters
{
    public class Scout : MonoBehaviour, IPlayer, IUpdatable, IArmed
    {
        #region Fields
        [Header("Settings")]
        [SerializeField]
        private float _speed = 5f;
        [SerializeField]
        private float _angularSpeed = 0;
        [Header("Components")]
        [SerializeField]
        private Rigidbody2D _rigidbody2D;
        [SerializeField]
        private Transform _weaponFolderLeft, _weaponFolderRight;
        [Header("Prefabs")]
        [SerializeField]
        private GameObject[] _weaponPrefabs;

        private UserMoveController moveController;
        private UserWeaponController weaponController;
        private IWeapon[] weapons;
        #endregion

        #region Props
        public float GetSpeed => _speed;
        public float GetAngularSpeed => _angularSpeed;
        public IWeapon[] GetWeapons => weapons;

        public Vector3 Position { get => _rigidbody2D.position; set => _rigidbody2D.position = value; }
        public Quaternion Rotation { get => transform.rotation; set => _rigidbody2D.SetRotation(value); }
        #endregion

        #region Events
        public event Action OnDie = () => { };
        public event Action OnKill = () => { };
        #endregion


        #region Private Methods
        private void Init()
        {
            weapons = Factory.getInstance.Create<IWeapon>(_weaponPrefabs, _weaponFolderLeft);
            moveController = new UserMoveController(this, _rigidbody2D, UIManager.getInstance.GetMainCamera);
            weaponController = new UserWeaponController(this);
        }
        #endregion

        #region Unity Methods
        void Start()
        {
            Init();
        }


        private void OnEnable()
        {
            TickManager.getInstance.OnTick += FastUpdate;
            TickManager.getInstance.OnFixedTick += FastFixedUpdate;
        }

        private void OnDisable()
        {
            TickManager.getInstance.OnTick -= FastUpdate;
            TickManager.getInstance.OnFixedTick -= FastFixedUpdate;
        }
        #endregion

        #region Public Methods
        public void FastUpdate()
        {
            moveController?.FastUpdate();
            weaponController?.FastUpdate();
        }

        public void FastFixedUpdate()
        {
            moveController?.FastFixedUpdate();
        }
        #endregion
    }
}