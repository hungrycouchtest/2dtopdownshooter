﻿using System;

namespace TopDownShooter.Characters
{
    public interface IPlayer : IMovable
    {
        event Action OnKill;
    }
}