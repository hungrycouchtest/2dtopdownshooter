﻿
using TopDownShooter.Weapons;

namespace TopDownShooter.Characters
{
    public interface IArmed
    {
        IWeapon[] GetWeapons { get; }
    }
}