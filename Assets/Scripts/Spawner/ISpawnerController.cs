﻿
namespace TopDownShooter.Others
{
    public interface ISpawnerController
    {
        bool IsActive { get; set; }

        void Spawn();
    }
}