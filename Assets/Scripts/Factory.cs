﻿using System.Collections.Generic;
using TopDownShooter.Misc;
using UnityEngine;

namespace TopDownShooter
{
    public class Factory : Singleton<Factory>
    {
        public void Create(GameObject prefab, Transform parent = null, Transform initialTransform = null)
        {
            var r = Instantiate(prefab);

            SetDefaultTransform(r.transform, parent, initialTransform);
        }

        public T Create<T>(GameObject prefab, Transform parent = null, Transform initialTransform = null)
        {
            var r = Instantiate(prefab).GetComponent<T>();

            SetDefaultTransform((r as Component).transform, parent, initialTransform);

            return r;
        }

        public T[] Create<T>(GameObject[] prefabs, Transform parent = null, Transform initialTransform = null)
        {
            List<T> result = new List<T>();

            foreach (var prefab in prefabs)
            {
                result.Add(Instantiate(prefab).GetComponent<T>());
                SetDefaultTransform((result[result.Count - 1] as Component).transform, parent, initialTransform);
            }

            return result.ToArray();
        }

        private void SetDefaultTransform(Transform transform, Transform parent, Transform initialTransform)
        {
            transform.SetParent(initialTransform != null ? initialTransform : parent);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.SetParent(parent);
        }
    }
}